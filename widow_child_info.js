const os = require('os');
const exec = require('child_process').exec
var mqtt = require('mqtt')
var mqtt_client  = mqtt.connect({ host: '54.208.84.23', port: '1500'})

mqtt_client.on('connect', function () {
console.log("Connection to MQTT Broker Successs ...");
})

var machine = new Object(); 


var getData = function()
{
  var nnn = os.networkInterfaces()
  var zzz= Object.keys(nnn);
  zzz.forEach(function(a)
  {
     if(a != 'lo')
     {
     	nnn[a].forEach(function(b){
     		if( b.family == 'IPv4')
     		{
     		 machine.id = "ID"+b.mac.replace(/:/g, '');
                 machine.mac = b.mac
     		 machine.ip = b.address
     		}
     	})
     } 
  })


  var cpu = os.cpus()
  machine.cpus = cpu.length
  machine.model = cpu[0].model


  machine.free_mem = os.freemem()
  machine.total_mem = os.totalmem()
  machine.arch = os.arch()
  machine.system  = os.platform()


  machine.useage_mem = machine.total_mem - machine.free_mem


  exec('dig +short myip.opendns.com @resolver1.opendns.com'
     	, function (error, stdout, stderr) {
    
    var public_ip = stdout.slice(0, -1);
    machine.public_ip = public_ip
    machine.datetime = new Date().toLocaleString()
    mqtt_client.publish('mqtt/status/in/'+machine.id , JSON.stringify(machine))
  });
}


setInterval(function(){
getData()
} , 3000)
